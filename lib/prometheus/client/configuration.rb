require 'prometheus/client/registry'
require 'prometheus/client/mmaped_value'
require 'logger'

module Prometheus
  module Client
    class Configuration
      attr_accessor :value_class, :multiprocess_files_dir, :initial_mmap_file_size, :logger, :pid_provider

      def initialize
        @value_class = ::Prometheus::Client::MmapedValue
        @multiprocess_files_dir = ENV['prometheus_multiproc_dir']
        @initial_mmap_file_size = 4 * 1024
        @logger = Logger.new($stdout)
        @pid_provider = Process.method(:pid)
      end
    end
  end
end
